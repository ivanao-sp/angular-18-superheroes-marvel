import { superheroe } from "./superheroe";

export interface superheroeDetalle extends superheroe {
    modificacion: string,
    comics: number,
    series: number,
    historias: number,
    eventos: number
}