import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialogTitle, MatDialogContent, MatDialogActions, MatDialogClose } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { modalData } from './../../../interfaces/modal-data';

@Component({
  selector: 'app-modal-mensaje',
  standalone: true,
  imports: [ MatButtonModule, MatDialogActions, MatDialogContent, MatDialogTitle, MatDialogClose ],
  templateUrl: './modal-mensaje.component.html',
  styleUrl: './modal-mensaje.component.scss'
})
export class ModalMensajeComponent {
  myData: modalData = {titulo: "", mensaje: ""}

  constructor( public dialogRef: MatDialogRef<ModalMensajeComponent>, @Inject(MAT_DIALOG_DATA) public data: modalData ) {
    this.myData = data; 
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
