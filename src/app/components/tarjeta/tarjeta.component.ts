import { Component, Input, Output, EventEmitter } from '@angular/core';
import { superheroe } from '../../interfaces/superheroe';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-tarjeta',
  standalone: true,
  imports: [ MatCardModule ],
  templateUrl: './tarjeta.component.html',
  styleUrl: './tarjeta.component.scss'
})
export class TarjetaComponent {

  @Input() superheroe: superheroe = { id: 0, nombre: "", imagen: ""};
  @Output() eventoVerDetalle = new EventEmitter<superheroe>();

  verDetalle() {
    this.eventoVerDetalle.emit(this.superheroe);
  }

}
