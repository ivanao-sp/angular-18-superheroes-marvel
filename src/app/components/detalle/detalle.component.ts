import { Component } from '@angular/core';
import { UpperCasePipe, DatePipe } from '@angular/common';
import { DataService } from '../../services/data/data.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { Subscription } from 'rxjs';
import { Router, RouterLink } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ModalMensajeComponent } from './../shared/modal-mensaje/modal-mensaje.component';
import { superheroeDetalle } from '../../interfaces/superheroe-detalle';

@Component({
  selector: 'app-detalle',
  standalone: true,
  imports: [ MatProgressSpinnerModule, MatIconModule, MatButtonModule, UpperCasePipe, DatePipe, RouterLink],
  templateUrl: './detalle.component.html',
  styleUrl: './detalle.component.scss'
})
export class DetalleComponent {
  private subscription: Subscription | null = null;
  banderaLoading: boolean = true;
  superheroe: superheroeDetalle = { id: 0, nombre: "", imagen: "", modificacion: "", comics: 0, series: 0, historias: 0, eventos: 0 };

  constructor(private dataService: DataService, private router: Router, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.getSuperheroe();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getSuperheroe() {
    
    this.subscription = this.dataService.get(true, "/"+this.router.url.split('/')[2]+"?").subscribe((data: any) => {
      
      this.superheroe.id = data.data.results[0].id;
      this.superheroe.nombre = data.data.results[0].name;
      this.superheroe.imagen = data.data.results[0].thumbnail.path+"."+data.data.results[0].thumbnail.extension;
      this.superheroe.modificacion = data.data.results[0].modified;
      this.superheroe.comics = data.data.results[0].comics.available;
      this.superheroe.series = data.data.results[0].series.available;
      this.superheroe.historias = data.data.results[0].stories.available;
      this.superheroe.eventos = data.data.results[0].events.available;
      
      this.banderaLoading = false;


    console.log("-----------------------------------------------------------------");
    console.log(this.superheroe)
    console.log("-----------------------------------------------------------------");  

    }, error => {
      this.ErrorPeticion();
    });
  }

  ErrorPeticion() {
    const dialogRef = this.dialog.open(ModalMensajeComponent, {
      data: {titulo: "ERROR", mensaje: "No se ha podido completar su petición."},
    });

  }

}
