import { Component, HostListener } from '@angular/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { superheroe } from '../../interfaces/superheroe';
import { DataService } from '../../services/data/data.service';
import { FormsModule } from '@angular/forms';
import { TarjetaComponent } from '../tarjeta/tarjeta.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ModalMensajeComponent } from './../shared/modal-mensaje/modal-mensaje.component';

@Component({
  selector: 'app-listado',
  standalone: true,
  imports: [ MatGridListModule, FormsModule, TarjetaComponent, MatProgressSpinnerModule, FormsModule, MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule, MatFormFieldModule, MatInputModule, FormsModule, MatButtonModule ],
  templateUrl: './listado.component.html',
  styleUrl: './listado.component.scss'
})
export class ListadoComponent {
  private subscription: Subscription | null = null;
  superherores: superheroe[] = [];
  limiteResultados: number = 9;
  inputBuscador = '';
  banderaLoading: boolean = true;
  banderaScroll: boolean = false;

  constructor(private dataService: DataService, private router: Router, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.getSuperheroes();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getSuperheroes() {
    let busqueda: string = "";
    if(this.inputBuscador != "") {
      busqueda = "nameStartsWith="+this.inputBuscador+"&";
    }
    this.subscription = this.dataService.get(false,"limit="+this.limiteResultados+"&"+busqueda).subscribe((data: any) => {
      this.limiteResultados = 9;
      this.superherores = data.data.results.map((item: any) => ({
        "id": item.id,
        "nombre": item.name,
        "imagen": item.thumbnail.path+"."+item.thumbnail.extension,
      }));
      this.banderaLoading = false;
      this.banderaScroll = false;

    }, error => {
      this.ErrorPeticion();
    });
  }

  @HostListener('window:scroll', ['$event'])
  eventoScroll(event: any): void {
    const element = event.target.scrollingElement;
    const atBottom = (element.scrollHeight - element.scrollTop) === element.clientHeight;
    if (atBottom) {
      const aux: number = this.limiteResultados + 9;
      if(this.limiteResultados != 100) {
        this.banderaScroll = true;
        if(aux > 100) {
          if(this.limiteResultados != 100) {
            this.limiteResultados = 100;
          }
        } else {
          this.limiteResultados = aux;
        }
        this.getSuperheroes(); 
      }
    }
  }
  
  verDetalle(superheroe: superheroe) {
    this.router.navigate(['/detalle/'+superheroe.id]);
  }

  ErrorPeticion() {
    const dialogRef = this.dialog.open(ModalMensajeComponent, {
      data: {titulo: "ERROR", mensaje: "No se ha podido completar su petición."},
    });

  }

}
