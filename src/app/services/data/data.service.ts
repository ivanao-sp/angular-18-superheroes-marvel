import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private static apikey: string = "6c8946719810c9fdebe8fb8e1f256b54";
  private static hash: string = "c1f5c12b69e67041f39a4975643f0510";
  private static url: string = "https://gateway.marvel.com/v1/public/characters";
  private static credenciales: string = "apikey="+DataService.apikey+"&hash="+DataService.hash+"&ts=1";

  constructor(private http: HttpClient) { }

  get(detalle: boolean, parametros: string = ""): Observable<any> {
    let auxURL: string = "";
    if(!detalle){ auxURL = "?" }
    return this.http.get(DataService.url+auxURL+parametros+DataService.credenciales);
  }

}
