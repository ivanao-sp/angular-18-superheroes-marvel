import { Routes } from '@angular/router';
import { ListadoComponent } from "./components/listado/listado.component";
import { DetalleComponent } from "./components/detalle/detalle.component"; 

export const routes: Routes = [
    { path: 'listado', component: ListadoComponent },
    { path: 'detalle/:id', component: DetalleComponent },
    { path: '**',  pathMatch: 'full', redirectTo: 'listado'},
];
